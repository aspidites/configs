if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
endif

"" Plugins
call plug#begin('~/.local/share/nvim/plugged')

" OS Detection
if !exists("g:os")
    let g:os = substitute(system('uname'), '\n', '', '')
endif


" Autocompletion
Plug 'roxma/nvim-cm-tern', { 'do': 'yarn' }
Plug 'ervandew/supertab'
Plug 'mattn/emmet-vim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'othree/csscomplete.vim'

" Linting
Plug 'dense-analysis/ale'
Plug 'tpope/vim-sleuth'


" Syntax highlighting
Plug 'sheerun/vim-polyglot'

" UI
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ajmwagar/vim-deus'

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }

Plug 'vmchale/dhall-vim'


" Navigation
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

call plug#end()

"" General Settings
set encoding=utf-8
" default indentation
set textwidth=80

" Keys
let mapleader=','

" UI
set list
set number
set showmatch
colorscheme deus
set background=dark
set colorcolumn=+1

" navigation
autocmd BufEnter * silent! lcd %:p:h

" searching
set ignorecase
set smartcase

" undo history
set undofile
set undodir=~/.config/nvim/undo

nnoremap <silent> <esc> :nohlsearch<cr>

"" Plugin Settings

" deoplete
let g:deoplete#enable_at_startup=1

" deus
let g:deus_termcolors=256

" supertab
let g:SuperTabDefaultCompletionType="<c-n>"

" airline
let g:airline#extension#tabline#enabled=1
let g:airline#extension#ale#enabled=1

let g:airline_powerline_fonts=1

" emmet
let g:user_emmet_expandabbr_key='<c-e>'
let g:use_emmet_complete_tag=1

" csscomplete
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS noci

" ale
let g:ale_linter={ 
\   'python': ['mypy', 'bandit', 'flake8'],
\   'javascript': ['eslint'],
\   'scss': ['stylelint']
\   }

let g:ale_fixers={
\   'javascript': ['eslint', 'prettier'],
\   'typescript': ['prettier', 'eslint'],
\   'haskell': ['brittany'],
\   'python': ['yapf'], 'php': ['phpcbf'],
\   'typescript.jsx': ['eslint', 'prettier'],
\   'scss': ['prettier', 'stylelint']
\ }

nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
nmap <silent> <C-l> :ALEFix<cr>

" syntastic
let g:syntastic_mode_map={
\   'mode': 'active',
\   'active_filetypes': ['purescript']
\ }

let g:syntastic_enable_racket_racket_checker=1

" psc-ide-vim
let g:psc_ide_syntastic_mode=1

" fzf
let g:fzf_buffers_jump=1
let g:fzf_tags_command='ctags -R'
let g:ale_emit_conflict_warnings=0

function! s:find_git_root()
  return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
endfunction


command! -bang -nargs=? -complete=dir Files
  \ call fzf#vim#files(<q-args>, fzf#vim#with_preview(), <bang>0)

command! ProjectFiles execute 'Files' s:find_git_root()

" live down
let g:mkdp_auto_start=1

" autosave
let g:auto_save_events=["InsertLeave"]

" terminal shortcuts
tnoremap <C-w>h <C-\><C-n><C-w>h
tnoremap <C-w>j <C-\><C-n><C-w>j
tnoremap <C-w>k <C-\><C-n><C-w>k
tnoremap <C-w>l <C-\><C-n><C-w>l

" force babelrc to be parsed as JSON
au BufNewFile,BufRead */.babelrc setfiletype javascript
