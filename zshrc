# disable auto-update
DISABLE_AUTO_UPDATE="true"

# enable zgen
source "${HOME}/.zgen/zgen.zsh"

# enable nix
#source /etc/profile.d/nix.sh

fpath+=~/.zfunc
source ~/.zprofile

# load zgen config
if ! zgen saved; then
    echo "Creating a zgen save..."

    zgen oh-my-zsh

    # plugins
    zgen oh-my-zsh plugins/git
    zgen oh-my-zsh plugins/sudo
    zgen oh-my-zsh plugins/colorize
    zgen oh-my-zsh plugins/poetry

    # completions
    zgen load zsh-users/zsh-completions src

    # theme
    zgen oh-my-zsh themes/arrow

    zgen save
fi

# aliases
unalias -m '*'

case `uname` in
    Darwin)
        alias ls='ls -G'
    ;;
    Linux)
        alias ls='ls --color=auto'
    ;;
esac

alias screenshare='vlc --no-video-deco --no-embedded-video --screen-fps=60 --screen-top=0 --screen-left=1920 --screen-width=1920 --screen-height=1080 screen://'
alias autolint='yapf -i $(git diff --name-only)'

# environment variables
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
export PATH=~/.cabal/bin:$PATH
export PATH=~/.local/bin:$PATH
export PATH=~/.cargo/bin:$PATH
export EDITOR=nvim
export BROWSER=chromium
export WINEARCH=win32
export RUST_SRC_PATH=~/projects/configs/rust/src
export N_PREFIX=~/.local

# disable shared history
unsetopt share_history

# rehash on completion
zstyle ':completion:*' rehash true

# faster completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

function update_db {
  dropdb clearui && \
  createdb clearui && \
  pg_restore -U clearui --dbname=clearui -Fc "$1"  && \
  psql -q -X -U clearui --dbname=clearui -c "update django_site set domain='sbx.clear.vm' where id=1;" && \
  python manage.py migrate
}

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_OPTS="--ansi --preview-window 'right:60%' --preview 'bat --color=always --style=header,grid --line-range :300 {}'"


change () {
    from=$1 
    shift
    to=$1 
    shift
    for file in $*
    do
        perl -i -p -e "s{$from}{$to}g;" $file
        echo "Changing $from to $to in $file"
    done
}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
export PATH=$HOME/.activecampaign/bin:$PATH
export PATH="/usr/local/opt/sphinx-doc/bin:$PATH"
export PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig"
export PATH="$HOME/.composer/vendor/bin/:$PATH"
if [[ -n $VIRTUAL_ENV ]]; then
    export PROMPT="📜 $PROMPT" 
fi

